import { performance } from 'perf_hooks'
import * as _ from 'lodash'

/**
 * Utils: getDuration
 * @param startTime timestamp
 */
export const getDuration = function (startTime: number) {
    if (startTime) {
        return performance.now() - startTime
    }
    return null
}


class SearchComputeProcessor {
    LOG_NAME = 'SearchComputeProcessor'

    static readonly DEFAULT_SHOULD_BOOST = 5
    static readonly SUGGESTS_HANDICAP = 0.3

    private mapFunc(key: string, value: any) {
        const startTime = performance.now()
        return new Promise((resolve, reject) => {
            let list = []
            _.forEach(value.data, (power) => {
                if (_.isNil(power._score)) {
                    power._score = 1
                }
                power._score = power._score * _.get(value, 'boost', SearchComputeProcessor.DEFAULT_SHOULD_BOOST)
                power.skill = key
                list.push([power.resource._key, power])
            })
            console.log(`${this.LOG_NAME}.mapFunc`, { func: this.LOG_NAME + '.mapFunc', took_ms: getDuration(startTime) })
            resolve(list)
        })
    }

    private reduceFunc(_key: string, values: any[]) {
        var sum = 0
        let resource = values[0].resource
        values.forEach(e => {
            sum += (e._score || 0)
        })
        return {
            resource: resource,
            score: sum,
            data: values
        }
    }

    async processResults(searchConfig, shouldResults) {
        const startTime = performance.now()
        let total = searchConfig.should.boost || SearchComputeProcessor.DEFAULT_SHOULD_BOOST
        let mapTasks = []
        let groups = null
        if (!shouldResults || shouldResults.length === 0) {
            console.info(`${this.LOG_NAME}.processResults`, { func: this.LOG_NAME + '.processResults', took_ms: getDuration(startTime) })
            return Promise.resolve([])
        }
        try {
            const results = await Promise.all(shouldResults)
            console.info(`${this.LOG_NAME}.processResults`, { func: this.LOG_NAME + '.processResults: before _.reduce', took_ms: getDuration(startTime) })
            console.log('RESULTS', results)
            let grouped = {}
            for (let i = 0; i < results.length; i++) {
                let el: [] = results[i] as []
                if (_.isArray(el) && el.length >= 2) {
                    if (!_.has(grouped, _.get(el, '[0]'))) {
                        grouped[_.get(el, '[0]')] = el
                    } else {
                        _.forEach(_.get(el, '[1].data', []), suggestions => { suggestions._score = suggestions._score * (1 - SearchComputeProcessor.SUGGESTS_HANDICAP); suggestions.suggests = true })
                        console.info(`${this.LOG_NAME}.processResults`, { func: this.LOG_NAME + '.processResults: before _.unionBy', took_ms: getDuration(startTime) })
                        grouped[_.get(el, '[0]')][1].data = _.unionBy(_.get(grouped[_.get(el, '[0]')], '[1].data', []), _.get(el, '[1].data', []), 'resource._key')
                        grouped[_.get(el, '[0]')][1].count = grouped[_.get(el, '[0]')][1].data.length
                        console.info(`${this.LOG_NAME}.processResults`, { func: this.LOG_NAME + '.processResults: after _.unionBy', took_ms: getDuration(startTime) })

                    }
                }
            }
            console.info(`${this.LOG_NAME}.processResults`, { func: this.LOG_NAME + '.processResults: after _.reduce', took_ms: getDuration(startTime) })
            let _results: [][] = _.values(grouped)
            console.info(`${this.LOG_NAME}.processResults`, { func: this.LOG_NAME + '.processResults: after _.values', took_ms: getDuration(startTime) })
            for (let i = 0; i < _results.length; i++) {
                if (_results[i] && _results[i].length >= 2) {
                    let value = _.get(_results[i], '[1]')
                    value.boost = _.get(_results[i], '[2]')
                    mapTasks.push(this.mapFunc(_.get(_results[i], '[0]'), value))
                }
            }

            console.info(`${this.LOG_NAME}.processResults`, { func: this.LOG_NAME + '.processResults: after _.forEach', took_ms: getDuration(startTime) })
            let result = await Promise.all(mapTasks)
            result = _.flatten(result)

            groups = {}
            for (let i = 0; i < result.length; i++) {
                let current = result[i]
                let group: any = []
                if (_.has(groups, current[0])) {
                    group = groups[current[0]]
                }
                group.push(current[1])
                groups[current[0]] = group
            }
            console.info(`${this.LOG_NAME}.processResults`, { func: this.LOG_NAME + '.processResults: after _.mapTasks', took_ms: getDuration(startTime) })

            for (var k in groups) {
                groups[k] = this.reduceFunc(k, groups[k])
                if (_.isNumber(groups[k].score)) {
                    groups[k].score = _.round(_.divide(groups[k].score, total), 5)
                }
            }
            console.info(`${this.LOG_NAME}.processResults`, { func: this.LOG_NAME + '.processResults', took_ms: getDuration(startTime) })
            return _.reverse(_.sortBy(_.values(groups), 'score'))
        } catch (error) {
            console.error(`${this.LOG_NAME}.processResults: unexpected error`, { error })
            throw error
        }
    }
}

/**
 * Generate test dataset
 * 
 */
function generateInputData () {
    let inputData = []
    let skills = ['SKI1', 'SKI2', 'SKI3', 'SKI4', 'SKI5', 'SKI6', 'SKI7']
    for (let skill of skills) {
        let inputSkillData = {count: 0, data: []}
            for (let i = 0; i < 100000; i++) {
                if (Math.random() > 0.25) {
                    inputSkillData.count ++
                    inputSkillData.data.push({
                        _key: 'POW' + i,
                        resource: {
                            _key: 'U' + i
                        },
                        skill: skill,
                        qualifiers: [
                            {
                                _key: 'SKQ1',
                                value: 2
                            },
                            {
                                _key: 'SKQ2',
                                value: ['test1', 'test2']
                            }
                        ],
                        _score: Math.random()
                    })
                }
            }
            inputData.push([
                skill,
                inputSkillData,
                5
            ])
    }
    return Promise.resolve(inputData)
}

/**
 * Exec code above
 */
async function execMapReduce() {
    let searchComputer = new SearchComputeProcessor()
    let inputData = await generateInputData()
    const results = await searchComputer.processResults({should: {boost: 4}}, inputData)
    // Log top 3 results
    console.log(_.take(results, 3))
}

execMapReduce()