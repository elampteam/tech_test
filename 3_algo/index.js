"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (this && this.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = op[0] & 2 ? y["return"] : op[0] ? y["throw"] || ((t = y["return"]) && t.call(y), 0) : y.next) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [op[0] & 2, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};
exports.__esModule = true;
var perf_hooks_1 = require("perf_hooks");
var _ = require("lodash");
/**
 * Utils: getDuration
 * @param startTime timestamp
 */
exports.getDuration = function (startTime) {
    if (startTime) {
        return perf_hooks_1.performance.now() - startTime;
    }
    return null;
};
var SearchComputeProcessor = /** @class */ (function () {
    function SearchComputeProcessor() {
        this.LOG_NAME = 'SearchComputeProcessor';
    }
    SearchComputeProcessor.prototype.mapFunc = function (key, value) {
        var _this = this;
        var startTime = perf_hooks_1.performance.now();
        return new Promise(function (resolve, reject) {
            var list = [];
            _.forEach(value.data, function (power) {
                if (_.isNil(power._score)) {
                    power._score = 1;
                }
                power._score = power._score * _.get(value, 'boost', SearchComputeProcessor.DEFAULT_SHOULD_BOOST);
                power.skill = key;
                list.push([power.resource._key, power]);
            });
            console.log(_this.LOG_NAME + ".mapFunc", { func: _this.LOG_NAME + '.mapFunc', took_ms: exports.getDuration(startTime) });
            resolve(list);
        });
    };
    SearchComputeProcessor.prototype.reduceFunc = function (_key, values) {
        var sum = 0;
        var resource = values[0].resource;
        values.forEach(function (e) {
            sum += (e._score || 0);
        });
        return {
            resource: resource,
            score: sum,
            data: values
        };
    };
    SearchComputeProcessor.prototype.processResults = function (searchConfig, shouldResults) {
        return __awaiter(this, void 0, void 0, function () {
            var startTime, total, mapTasks, groups, results, grouped, i, el, _results, i, value, result, i, current, group, k, error_1;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        startTime = perf_hooks_1.performance.now();
                        total = searchConfig.should.boost || SearchComputeProcessor.DEFAULT_SHOULD_BOOST;
                        mapTasks = [];
                        groups = null;
                        if (!shouldResults || shouldResults.length === 0) {
                            console.info(this.LOG_NAME + ".processResults", { func: this.LOG_NAME + '.processResults', took_ms: exports.getDuration(startTime) });
                            return [2 /*return*/, Promise.resolve([])];
                        }
                        _a.label = 1;
                    case 1:
                        _a.trys.push([1, 4, , 5]);
                        return [4 /*yield*/, Promise.all(shouldResults)];
                    case 2:
                        results = _a.sent();
                        console.info(this.LOG_NAME + ".processResults", { func: this.LOG_NAME + '.processResults: before _.reduce', took_ms: exports.getDuration(startTime) });
                        console.log('RESULTS', results);
                        grouped = {};
                        for (i = 0; i < results.length; i++) {
                            el = results[i];
                            if (_.isArray(el) && el.length >= 2) {
                                if (!_.has(grouped, _.get(el, '[0]'))) {
                                    grouped[_.get(el, '[0]')] = el;
                                }
                                else {
                                    _.forEach(_.get(el, '[1].data', []), function (suggestions) { suggestions._score = suggestions._score * (1 - SearchComputeProcessor.SUGGESTS_HANDICAP); suggestions.suggests = true; });
                                    console.info(this.LOG_NAME + ".processResults", { func: this.LOG_NAME + '.processResults: before _.unionBy', took_ms: exports.getDuration(startTime) });
                                    grouped[_.get(el, '[0]')][1].data = _.unionBy(_.get(grouped[_.get(el, '[0]')], '[1].data', []), _.get(el, '[1].data', []), 'resource._key');
                                    grouped[_.get(el, '[0]')][1].count = grouped[_.get(el, '[0]')][1].data.length;
                                    console.info(this.LOG_NAME + ".processResults", { func: this.LOG_NAME + '.processResults: after _.unionBy', took_ms: exports.getDuration(startTime) });
                                }
                            }
                        }
                        console.info(this.LOG_NAME + ".processResults", { func: this.LOG_NAME + '.processResults: after _.reduce', took_ms: exports.getDuration(startTime) });
                        _results = _.values(grouped);
                        console.info(this.LOG_NAME + ".processResults", { func: this.LOG_NAME + '.processResults: after _.values', took_ms: exports.getDuration(startTime) });
                        for (i = 0; i < _results.length; i++) {
                            if (_results[i] && _results[i].length >= 2) {
                                value = _.get(_results[i], '[1]');
                                value.boost = _.get(_results[i], '[2]');
                                mapTasks.push(this.mapFunc(_.get(_results[i], '[0]'), value));
                            }
                        }
                        console.info(this.LOG_NAME + ".processResults", { func: this.LOG_NAME + '.processResults: after _.forEach', took_ms: exports.getDuration(startTime) });
                        return [4 /*yield*/, Promise.all(mapTasks)];
                    case 3:
                        result = _a.sent();
                        result = _.flatten(result);
                        groups = {};
                        for (i = 0; i < result.length; i++) {
                            current = result[i];
                            group = [];
                            if (_.has(groups, current[0])) {
                                group = groups[current[0]];
                            }
                            group.push(current[1]);
                            groups[current[0]] = group;
                        }
                        console.info(this.LOG_NAME + ".processResults", { func: this.LOG_NAME + '.processResults: after _.mapTasks', took_ms: exports.getDuration(startTime) });
                        for (k in groups) {
                            groups[k] = this.reduceFunc(k, groups[k]);
                            if (_.isNumber(groups[k].score)) {
                                groups[k].score = _.round(_.divide(groups[k].score, total), 5);
                            }
                        }
                        console.info(this.LOG_NAME + ".processResults", { func: this.LOG_NAME + '.processResults', took_ms: exports.getDuration(startTime) });
                        return [2 /*return*/, _.reverse(_.sortBy(_.values(groups), 'score'))];
                    case 4:
                        error_1 = _a.sent();
                        console.error(this.LOG_NAME + ".processResults: unexpected error", { error: error_1 });
                        throw error_1;
                    case 5: return [2 /*return*/];
                }
            });
        });
    };
    SearchComputeProcessor.DEFAULT_SHOULD_BOOST = 5;
    SearchComputeProcessor.SUGGESTS_HANDICAP = 0.3;
    return SearchComputeProcessor;
}());
/**
 * Generate test dataset
 *
 */
function generateInputData() {
    var inputData = [];
    var skills = ['SKI1', 'SKI2', 'SKI3', 'SKI4', 'SKI5', 'SKI6', 'SKI7'];
    for (var _i = 0, skills_1 = skills; _i < skills_1.length; _i++) {
        var skill = skills_1[_i];
        var inputSkillData = { count: 0, data: [] };
        for (var i = 0; i < 100000; i++) {
            if (Math.random() > 0.25) {
                inputSkillData.count++;
                inputSkillData.data.push({
                    _key: 'POW' + i,
                    resource: {
                        _key: 'U' + i
                    },
                    skill: skill,
                    qualifiers: [
                        {
                            _key: 'SKQ1',
                            value: 2
                        },
                        {
                            _key: 'SKQ2',
                            value: ['test1', 'test2']
                        }
                    ],
                    _score: Math.random()
                });
            }
        }
        inputData.push([
            skill,
            inputSkillData,
            5
        ]);
    }
    return Promise.resolve(inputData);
}
/**
 * Exec code above
 */
function execMapReduce() {
    return __awaiter(this, void 0, void 0, function () {
        var searchComputer, inputData, results;
        return __generator(this, function (_a) {
            switch (_a.label) {
                case 0:
                    searchComputer = new SearchComputeProcessor();
                    return [4 /*yield*/, generateInputData()];
                case 1:
                    inputData = _a.sent();
                    return [4 /*yield*/, searchComputer.processResults({ should: { boost: 4 } }, inputData)
                        // Log top 3 results
                    ];
                case 2:
                    results = _a.sent();
                    // Log top 3 results
                    console.log(_.take(results, 3));
                    return [2 /*return*/];
            }
        });
    });
}
execMapReduce();
